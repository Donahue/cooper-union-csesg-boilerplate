import React from "react";

class Wheel_2 extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }



  One() {
    
    this.props.callbackFromParent(this.props.ops.answers[0].a, this.props.ops.answers[0].next);
  }

  Two() {
        this.props.callbackFromParent(this.props.ops.answers[1].a, this.props.ops.answers[1].next,);
  }


  render() {
    console.log(this.props.ops)
    return (
      <div>
        <h1 id="HEADER">{this.props.ops.question}</h1>



        <div
            id="One"
            className="halfCircleBottom"
          onClick={() => {
            this.One();
          }}
        >
          <div class="content">
            <b>{this.props.ops.answers[0].a}</b>
          </div>
        </div>



        <div
            id="Two"
            class="halfCircleTop"
          onClick={() => {
            this.Two();
          }}
        >
          <div class="content">
            <b>{this.props.ops.answers[1].a}</b>
          </div>
        </div>


              </div>
    );
  }
}

export default Wheel_2;
