
import React from 'react';
import Grid from '@material-ui/core/Grid';

class GoalQuestion extends React.Component{
	render(){
		return(
			<div>
				<form>
				<Grid container spacing={50}>
					<Grid item xs={2} id="goalCard">
						<p id="submessage">Where are you right now?</p>
					</Grid>

					<Grid item xs={1}>
					</Grid>

					<Grid item xs={2} id="goalCard">
						<p id="submessage">Where would you like to go next?</p>
						<input type="text"/>
					</Grid>

					<Grid item xs={1}>
					</Grid>

					<Grid item xs={2} id="goalCard">
						<p id="submessage">Where would you like to end up?</p>
						<input type="text"/>
					</Grid>

					<Grid item xs={1}>
					</Grid>

					<Grid item xs={2} id="goalCard">
						<input type="submit" value="Submit"/>
					</Grid>
				</Grid>
				</form>

				<br/>

				<button className="signButton">I'm not sure!</button>
			</div>
		)
	}
};

export default GoalQuestion