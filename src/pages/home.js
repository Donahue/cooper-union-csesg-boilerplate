import React from 'react';
import { Link } from 'react-router-dom';
import NavButton from '../components/navbutton.js';
import TimeLine from '../components/timeline.js';
import NavBar from '../components/navbar.js';
import AnchorLink from 'react-anchor-link-smooth-scroll';
import Grid from '@material-ui/core/Grid';
import TimelineEvent from "../components/timeline-event.js";
import { compose } from 'redux';
import { connect } from 'react-redux';
import Results from '../components/results.js';
import { firebaseConnect, isLoaded, isEmpty } from 'react-redux-firebase';
import Button from '@material-ui/core/Button';
import PropTypes from 'prop-types';
import FormControl from '@material-ui/core/FormControl';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { Redirect } from 'react-router-dom'

const styles = {
  button: {
    borderRadius: 7,
    width: 100,
    height: 50,
    color: 'white',
    background: '#013243',
    '&:hover': {
        color: '#013243',
        background: '#F6F6F6',
    }
  },
};

class HomePage extends React.Component{

	constructor(props){
		super(props);
		this.state = {
			 events: [

		{
			title: 'Freshman',
			desc: 'First Year of HS'
		}, 


		{
			title: 'Doctor',
			desc: 'Work in the med field'
		},
		

	],

	recommendations: [
		{
			title: 'Reccommendation 1',
			desc: 'This reccomendation has been generated in back-end'

		},
		{
			title: 'Reccomendation 2',
			desc: 'This reccomendation has been generated in back-end'
		},
		
		{
			title: 'Reccomendation 3',
			desc: 'This reccomendation has been generated in back-end'
		},
	]
		}
	}

	
    	addEvent(num){
    		console.log(num);
		this.setState({events: [...this.state.events, this.state.recommendations[num] ]});
		console.log(this.state);
	}
logout(){
    this.props.firebase.logout();
    }


    render(){
    	
		console.log(this.props);
		let payload;
			if(!this.props.auth.isLoaded){
				payload = null;
			}
			

			
			if(this.props.auth.isLoaded && this.props.auth.isEmpty){   //not logged in
				payload = 
						<div>

	    <div>
			<header>
				<p id="title"><a href="/HomePage" style={{float:"left", marginLeft:400, fontSize:45, fontWeight: "bold"}}>My Career Timeline</a>

				
			</p>
			
			<p id="title">
				<Link to="/login">Log In</Link>
				<AnchorLink href='#card4'>How It Works</AnchorLink>
	    		<AnchorLink href='#card3'>Our Story</AnchorLink>
	    		<AnchorLink href='#card2'>Mission</AnchorLink>
		    	<AnchorLink href='#card1'>Demo</AnchorLink>
		    	</p>
				
	    	</header>
	    </div>

	    <div>
	    	<Grid container id="card1">
	    		<Grid item xl={3}>
	    			<img className="mountains" src="https://www.noahdigital.ca/wp-content/uploads/2018/01/our-mission-icon.png" />
	    		</Grid>
	    		<Grid item xs={1}>
	    		</Grid>
	    		<Grid item lg={6}>
	    			<h1 id="message">The smarter way to succeed.</h1>
	   				<p id="submessage">Create and organize timelines that auto-populate based on the goals you want to achieve.</p>
	   				<Link to="/signInteractives"><button className="demoButton">Try the Demo</button></Link> 
					
					
	    		</Grid>
	    	</Grid>
	   	</div>


	   	<div>
	   		<Grid container id="card2">
	   			<Grid item md={5}>
	   				<h1 id="message">Our Mission:</h1>
	   				<p id="submessage">Our mission is to provide career guidance to indivudals who would otherwise not have access to career resources and information. We provide detailed, accurate, career information, and allow our users to explore different careers comparing projected salaries, educational expenses, and ideal location. Based on their current situation and their long term career goals, we generate a customized timeline, showing our user exactly how to get wherever they want to go. Together, we can jumpstart the future entrepeneurs, athletes, and everything in between!</p>
	   			</Grid>
	   			<Grid item xl={6}>
	   				<img src="http://webinarmasterysummit.com/wp-content/uploads/2015/11/idea-rocket-e1449125760807.png"/>
	   			</Grid>
	   		</Grid>
	   	</div>

	   	<div>
	   		<Grid container id="card3">
	   			<Grid item xs={12}>
	   				<h1 id="message">Our Story:</h1>
	   			</Grid>
	   			<Grid item xs={4}>
	   				<img src="https://png.icons8.com/color/1600/person-female.png" id="icon"/>
	   				<p id="submessage"><b>Faria</b> completed her undergraduate in New York at NYU, where she started a college access organization called Project College which helped over a thousand underserved high school students in NYC apply for college and financial aid. For her work, she won the NYU Reynolds changemaker challenge first place award in 2014. Since then, She has been working at Intel in Finance and is pursuing her Masters in Data Science at UC Berkeley.</p>
	   			</Grid>
	   			<Grid item xs={4}>
	   				<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d1/Icons8_flat_businessman.svg/1024px-Icons8_flat_businessman.svg.png" id="icon"/>
					<p id="submessage"><b>Harsh</b> has a Masters degree in Computer Science, and 6 years of experience in the field of software development. He is very passionate about social justice and hopes to use his skills in computer science to improve global problems. Harsh is currently working at eBay in Seattle.</p>
	   			</Grid>
	   			<Grid item xs={4}>
	   				<img src="http://2017.igem.org/wiki/images/b/bd/AQAUnesp-team-icon.png" id="icon"/>
	   				<p id="submessage"><b>Amy, Austin, Erin, Sira, and Tara</b> are rising juniors, attending Cooper Union's Summer STEM program: Computer Science for Social Good. While they do not have as much experience as Harsh and Faria, as High School students they understand many of the difficulties of navigating higher educatoin and career planning.</p>
	   			</Grid>
	   			<Grid item xs={12}>
	   				<p id="submessage">Together we wanted to continue to work on the issue of college access and career guidance for underserved students, but using our technical skills to have more impact.</p>
	   			</Grid>
   			</Grid>
	   	</div>

	   	<div>
	   		<Grid container id="card4">
	   			
				<Grid item md={6}>
	   				<h1 id="message">How It Works:</h1>
	   				<p id="submessage">
					
					
					
					
				
					
					
					
					</p>
	   			</Grid>
				
			
	   			<Grid item xl={6}>
	   			</Grid>
				
				
	   		</Grid>
	   	</div>


	</div>
			}
			
		if(this.props.auth.isLoaded && !this.props.auth.isEmpty){ //is logged in
				payload = 
						<div>

	    <div>
	    	<header>
				<p id="title"><a href="/HomePage" style={{float:"left", marginLeft:400, fontSize:45, fontWeight: "bold"}}>My Career Timeline</a>

				
			</p>
			
			<p id="title">
			<Link to="/HomePage"><button variant="contained"
			
			    color="secondary" 
				style={{fontWeight:"bold", fontSize:20, padding:"0 0 0 0"}}
			    onClick={() => {this.props.firebase.logout();}}>
			Logout
		    </button></Link>
				<Link to="/profile">My Profile</Link>
				<AnchorLink href='#card4'>How It Works</AnchorLink>
	    		<AnchorLink href='#card3'>Our Story</AnchorLink>
	    		<AnchorLink href='#card2'>Mission</AnchorLink>
		    	<AnchorLink href='#card1'>Demo</AnchorLink>
		    	</p>
				
	    	</header>
			
			
	    </div>

	    <div>
	    	<Grid container id="card1">
	    		<Grid item xl={3}>
	    			<img className="mountains" src="https://www.noahdigital.ca/wp-content/uploads/2018/01/our-mission-icon.png" />
	    		</Grid>
	    		<Grid item xs={1}>
	    		</Grid>
	    		<Grid item lg={6}>
	    			<h1 id="message">The smarter way to succeed.</h1>
	   				<p id="submessage">Create and organize timelines that auto-populate based on the goals you want to achieve.</p>
	   				<Link to="/signInteractives"><button className="demoButton">Try the Demo</button></Link> 
					
					
	    		</Grid>
	    	</Grid>
	   	</div>


	   	<div>
	   		<Grid container id="card2">
	   			<Grid item md={5}>
	   				<h1 id="message">Our Mission:</h1>
	   				<p id="submessage">Our mission is to provide career guidance to indivudals who would otherwise not have access to career resources and information. We provide detailed, accurate, career information, and allow our users to explore different careers comparing projected salaries, educational expenses, and ideal location. Based on their current situation and their long term career goals, we generate a customized timeline, showing our user exactly how to get wherever they want to go. Together, we can jumpstart the future entrepeneurs, athletes, and everything in between!</p>
	   			</Grid>
	   			<Grid item xl={6}>
	   				<img src="http://webinarmasterysummit.com/wp-content/uploads/2015/11/idea-rocket-e1449125760807.png"/>
	   			</Grid>
	   		</Grid>
	   	</div>

	   	<div>
	   		<Grid container id="card3">
	   			<Grid item xs={12}>
	   				<h1 id="message">Our Story:</h1>
	   			</Grid>
	   			<Grid item xs={4}>
	   				<img src="https://png.icons8.com/color/1600/person-female.png" id="icon"/>
	   				<p id="submessage"><b>Faria</b> completed her undergraduate in New York at NYU, where she started a college access organization called Project College which helped over a thousand underserved high school students in NYC apply for college and financial aid. For her work, she won the NYU Reynolds changemaker challenge first place award in 2014. Since then, She has been working at Intel in Finance and is pursuing her Masters in Data Science at UC Berkeley.</p>
	   			</Grid>
	   			<Grid item xs={4}>
	   				<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d1/Icons8_flat_businessman.svg/1024px-Icons8_flat_businessman.svg.png" id="icon"/>
					<p id="submessage"><b>Harsh</b> has a Masters degree in Computer Science, and 6 years of experience in the field of software development. He is very passionate about social justice and hopes to use his skills in computer science to improve global problems. Harsh is currently working at eBay in Seattle.</p>
	   			</Grid>
	   			<Grid item xs={4}>
	   				<img src="http://2017.igem.org/wiki/images/b/bd/AQAUnesp-team-icon.png" id="icon"/>
	   				<p id="submessage"><b>Amy, Austin, Erin, Sira, and Tara</b> are rising juniors, attending Cooper Union's Summer STEM program: Computer Science for Social Good. While they do not have as much experience as Harsh and Faria, as High School students they understand many of the difficulties of navigating higher educatoin and career planning.</p>
	   			</Grid>
	   			<Grid item xs={12}>
	   				<p id="submessage">Together we wanted to continue to work on the issue of college access and career guidance for underserved students, but using our technical skills to have more impact.</p>
	   			</Grid>
   			</Grid>
	   	</div>

	   	<div>
	   		<Grid container id="card4">
	   			
				<Grid item md={6}>
	   				<h1 id="message">How It Works:</h1>
	   				<p id="submessage">
					
					
					
					
				
					
					
					
					</p>
	   			</Grid>
				
				<Grid item xl={6}>
	   			</Grid>
				
	   		</Grid>
	   	</div>


	</div>
	
		};
	return(
	<div>
	{payload}
	</div>
	)
	}
};

export default withStyles(styles)(compose(
    firebaseConnect(),
    connect(({firebase: {auth}}) => ({auth})),
    connect(({ firebase: { profile } }) => ({ profile }))
)(HomePage));