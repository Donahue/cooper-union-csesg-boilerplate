import React from 'react';
import { Link } from 'react-router-dom';
import TimeLine from '../components/timeline.js';

export default class MyTimelines extends React.Component{
    render(
    	
){
	return(
	    <div>
		<h1>
		    My Timelines
		</h1>
		<TimeLine/>
		<Link to="/another_page">Another Page</Link>
    
	    </div>
	)
    }
};

